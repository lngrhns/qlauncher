package de.langerhans.android.qlauncher

import android.content.ComponentName
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import kotlinx.android.synthetic.main.applist_item.view.*
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.support.v7.graphics.Palette
import android.widget.FrameLayout

class ApplistAdapter : RecyclerView.Adapter<ApplistAdapter.ApplistViewHolder>() {

    var items : List<ApplistViewModel> = emptyList()

    override fun getItemCount() = items.size

    override fun onBindViewHolder(viewHolder: ApplistViewHolder, position: Int) {
        val data = items[position]
        val pm = viewHolder.icon.context.packageManager
        val intent = Intent().also {
            it.component = ComponentName(data.packageName, data.activityName)
        }
        val resolveInfo = pm.resolveActivity(intent, 0)

        val icon = resolveInfo.loadIcon(pm)
        val palette = Palette.from(icon.toBitmap()).generate()

        viewHolder.bg.setBackgroundColor(palette.getLightMutedColor(Color.GRAY))
        viewHolder.icon.setImageDrawable(icon)
        viewHolder.root.setOnClickListener {
            val launchIntent =
                viewHolder.root.context.packageManager.getLaunchIntentForPackage(data.packageName)
            viewHolder.root.context.startActivity(launchIntent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ApplistViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.applist_item, parent, false)
        return ApplistViewHolder(view)
    }

    class ApplistViewHolder(val root: View): RecyclerView.ViewHolder(root) {
        val icon: ImageView = root.applistIcon
        val bg: FrameLayout = root.applistIconBg
    }

}