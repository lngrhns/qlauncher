package de.langerhans.android.qlauncher

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.content.Intent
import kotlinx.android.synthetic.main.activity_launcher.*


class LauncherActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)

        val intent = Intent(Intent.ACTION_MAIN, null).also {
            it.addCategory(Intent.CATEGORY_LAUNCHER)
        }

        val apps = packageManager.queryIntentActivities(intent, 0).map {
            ApplistViewModel(it.activityInfo.packageName, it.activityInfo.name, it.loadLabel(packageManager).toString())
        }.sortedBy { it.label.toLowerCase() }

        appList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        val adapter = ApplistAdapter()
        adapter.items = apps
        appList.adapter = adapter
    }
}
