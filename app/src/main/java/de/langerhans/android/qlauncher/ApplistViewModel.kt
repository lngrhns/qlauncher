package de.langerhans.android.qlauncher

data class ApplistViewModel(
    val packageName: String,
    val activityName: String,
    val label: String
)